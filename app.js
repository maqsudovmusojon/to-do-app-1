$(document).ready(function (){
    function getCurrentTime() {
        var date = new Date();
        var day = date.getDate(),
            month = date.getMonth() + 1,
            year = date.getFullYear(),
            hour = date.getHours(),
            min = date.getMinutes();
        month = (month < 10 ? "0" : "") + month;
        day = (day < 10 ? "0" : "") + day;
        hour = (hour < 10 ? "0" : "") + hour;
        min = (min < 10 ? "0" : "") + min;
        var today = year + "-" + month + "-" + day;
        var displayTime = hour + ":" + min;
        $('#form-for-date input').val(today);
        $("#form-for-time input").val(displayTime);
    }
    function removeCurrentTime(){
        $("#to-do-due-date").val([]);
        $("#to-do-time").val([]);
    }
    function getActiveMenu(){
        $("#task-list-menu, #task-page-menu,#new-task-menu, #completed-tasks-menu, #editing-of-a-task-menu, #adding-a-comment-menu").removeClass("active-left-menu");
        $(this).addClass("active-left-menu");
    }
    function getActiveToDoHolder(){
        $(".to-do-list, .to-do-list-add-holder, .to-do-list-page, .to-do-list-edit, .to-do-completed-tasks, .to-do-adding-a-comment").hide();
        switch (this.id) {
            case "task-list-menu": $(".to-do-list").show(); break;
            case "task-page-menu": $(".to-do-list-page").show(); break;
            case "new-task-menu": {
                $(".to-do-list-add-holder").show();
                getCurrentTime();
                break;
            }
            case "completed-tasks-menu": $(".to-do-completed-tasks").show(); break;
            case "editing-of-a-task-menu": $(".to-do-list-edit").show(); break;
            case "adding-a-comment-menu":  $(".to-do-adding-a-comment").show(); break;
        }
    }
    function getToggleCheck(){
        $(this).toggleClass("fa-check-square");
        $(this).toggleClass("fa-square");
    }
    function getToggleShowText(){
        if (this.text() === "Show comments")
            this.text("Hide comments");
        else
            this.text("Show comments");
    }
    function removeElementOfTableTR(){
        $(this).parents("tr").fadeOut(250);
        $(this).delay(350).queue(function (){
            $(this).parents("tr").remove();
            $(this).dequeue();
        })
    }
    function clearAllElementsTR(){
        $(this).fadeOut(250);
        $(this).delay(350).queue(function (){
            $(this).parents("tr").remove();
            $(this).dequeue();
        })
    }

    $(document).on("click", ".left-menu-bar div:not(.left-menu-bar div:first-child)", function (){
        getActiveMenu.call(this);
        getActiveToDoHolder.call(this);
    });

                                  /* This all for TASK LIST MENU  */
    $(".to-do-list table tbody tr td:first-child i").click(function (){
        getToggleCheck.call(this);
        removeElementOfTableTR.call(this);

    }); /* Mark as completed function */
    $(".to-do-list table thead tr th:last-child .dropdown .dropdown-content a:first-child").click(function (){
        getActiveMenu.call();
        $("#editing-of-a-task-menu").addClass('active-left-menu');
        getActiveToDoHolder.call();
        $(".to-do-list-edit").show();

    }); /* This make menu bar to move to edit bar */
    $(".to-do-list table thead tr th:last-child .dropdown .dropdown-content a:last-child").click(function (){
        var x = $(".to-do-list table thead tr th:last-child .dropdown .dropdown-content a:last-child");
        $(".to-do-list table tbody tr td:nth-child(2) p.form-for-comment-1").toggle();
        getToggleShowText.call(this);

    }); /* This toggle show hide of COMMENT */
                                  /* ************************* */

                                  /* This all for TASK PAGE MENU  */
    $(".to-do-list-page table thead tr th:last-child .dropdown .dropdown-content a").click(function (){
        var x = $(".to-do-list-page table thead tr th:last-child .dropdown .dropdown-content a");
        $(".to-do-list-page table tbody tr td:nth-child(2) p.form-for-comment-1").toggle();
        getToggleShowText.call(".to-do-list-page table thead tr th:last-child .dropdown .dropdown-content a");
    });/* This toggle show hide of COMMENT */
                                 /* ************************* */

                                 /* This all for NEW TASK MENU */
    $("#new-task-menu").click(function (){
       $("#current-time").hide();
    });
    $(document).on("click", "#save-button", function (){
        if ($("#to-do-add").val() === "")
            $(".span-for-input").addClass("span-border-red");

        else if ($("#to-do-add"). val()){
            $(".span-for-input").removeClass("span-border-red");
            var time = $("#to-do-time").val();
            var date = $("#to-do-due-date").val();
            date = date.split("-")[2] + "-" + date.split("-")[1];
            /*switch (Number(date.split("-")[1])){
                case 1: date1 = "January"; break;
                case 2: date1 = "February"; break;
                case 3: date1 = "March"; break;
                case 4: date1 = "April"; break;
                case 5: date1 = "May"; break;
                case 6: date1 = "June"; break;
                case 7: date1 = "July"; break;
                case 8: date1 = "August"; break;
                case 9: date1 = "September"; break;
                case 10: date1 = "October"; break;
                case 11: date1 = "November"; break;
                case 12: date1 = "December"; break;
            }*/
            /*date = date.split("-")[2] + "" + date1;*/
            var priority = $("#to-do-priority").val();
            if (priority === 0) priority = "<i class=\"fas fa-circle\"></i><p>No Priority</p>";
            else if (priority === 1) priority = "<i class=\"fas fa-circle high-priority\"></i><p>High Priority</p>";
            else if(priority === 2) priority = "<i class=\"fas fa-circle medium-priority\"></i><p>Medium Priority</p>";
            else priority = "<i class=\"fas fa-circle low-priority\"></i><p>Low Priority</p>";

            $(".to-do-list .content-table tbody").append(
                "                       <tr class=\"tr-hover-effect\" >\n" +
                "                            <td><i class=\"far fa-square\"></i></td>\n" +
                "                            <td><p>"+ $("#to-do-add").val() +"</p><p style=\"display: none\" class=\"form-for-comment-1\" >No comment yet</p></td>\n" +
                "                            <td><div class=\"date-time-todo\"><p>"+ time +" </p><span></span><p class=\"date-todo\">"+ date + "</p></div></td>\n " +
                "                            <td> "+ priority +" </td>\n" +
                "                            <td><div class=\"message-received-status-holder\">\n" +
                "                                <i class=\"far fa-comment-alt\"></i>\n" +
                "                                <div class=\"message-received-status message-received-status-bc-1\">2</div>\n" +
                "                            </div></td>\n" +
                "                            <td><div class=\"in-progress-div\">In Progress</div></td>\n" +
                "                        </tr>");

            $(".to-do-list-page .content-table tbody").append(
                "                       <tr class=\"tr-hover-effect\" >\n" +
                "                            <td></td>\n" +
                "                            <td><p>"+ $("#to-do-add").val() +"</p><p style=\"display: none\" class=\"form-for-comment-1\" >No comment yet</p></td>\n" +
                "                            <td><div class=\"date-time-todo\"><p>"+ time +" </p><span></span><p class=\"date-todo\">"+ date + "</p></div></td>\n " +
                "                            <td> "+ priority +" </td>\n" +
                "                            <td><div class=\"message-received-status-holder\">\n" +
                "                                <i class=\"far fa-comment-alt\"></i>\n" +
                "                                <div class=\"message-received-status message-received-status-bc-1\">2</div>\n" +
                "                            </div></td>\n" +
                "                            <td><div class=\"in-progress-div\">In Progress</div></td>\n" +
                "                        </tr>");

            $(".to-do-list-edit .content-table tbody").append(
                "                       <tr class=\"tr-hover-effect\" >\n" +
                "                            <td></td>\n" +
                "                            <td><p>"+ $("#to-do-add").val() +"</p></td>\n" +
                "                            <td><div class=\"date-time-todo\"><p>"+ time +" </p><span></span><p class=\"date-todo\">"+ date + "</p></div></td>\n " +
                "                            <td> "+ priority +" </td>\n" +
                "                            <td><button type=\"button\" class=\"btn btn-info\">Edit</button></td>\n" +
                "                            <td><button type=\"button\" class=\"btn btn-danger\">Delete</button></td>\n" +
                "                        </tr>");

            $(".to-do-adding-a-comment .content-table tbody").append(
                "                       <tr class=\"tr-hover-effect\" >\n" +
                "                            <td></td>\n" +
                "                            <td><p>"+ $("#to-do-add").val() +"</p><input type=\"text\" class=\"form-for-comment\" placeholder=\"Write a comment here..\"></td>\n" +
                "                            <td><div class=\"date-time-todo\"><p>"+ time +" </p><span></span><p class=\"date-todo\">"+ date + "</p></div></td>\n " +
                "                            <td> "+ priority +" </td>\n" +
                "                            <td><button type=\"button\" class=\"btn btn-info\">Save</button></td>\n" +
                "                            <td><button type=\"button\" class=\"btn btn-info\">Cancel</button></td>\n" +
                "                        </tr>");
        }


        $("#to-do-add").val([]);
        $("#to-do-priority option").prop('selected', false);

    });
    $(document).on("click", "#save-nav-button", function (){
        if ($("#to-do-add").val() === "")
            $(".span-for-input").addClass("span-border-red");

        else if ($("#to-do-add"). val()){
            $(".span-for-input").removeClass("span-border-red");
            var time = $("#to-do-time").val();
            var date = $("#to-do-due-date").val();
            date = date.split("-")[2] + "-" + date.split("-")[1];

            /*switch (Number(date.split("-")[1])){
                case 1: date1 = "January"; break;
                case 2: date1 = "February"; break;
                case 3: date1 = "March"; break;
                case 4: date1 = "April"; break;
                case 5: date1 = "May"; break;
                case 6: date1 = "June"; break;
                case 7: date1 = "July"; break;
                case 8: date1 = "August"; break;
                case 9: date1 = "September"; break;
                case 10: date1 = "October"; break;
                case 11: date1 = "November"; break;
                case 12: date1 = "December"; break;
            }*/
            var priority = $("#to-do-priority").val();
            if (priority == 0) priority = "<i class=\"fas fa-circle\"></i><p>No Priority</p>";
            else if (priority == 1) priority = "<i class=\"fas fa-circle high-priority\"></i><p>High Priority</p>";
            else if(priority == 2) priority = "<i class=\"fas fa-circle medium-priority\"></i><p>Medium Priority</p>";
            else priority = "<i class=\"fas fa-circle low-priority\"></i><p>Low Priority</p>";

            $(".to-do-list .content-table tbody").append(
                "                       <tr class=\"tr-hover-effect\" >\n" +
                "                            <td><i class=\"far fa-square\"></i></td>\n" +
                "                            <td><p>"+ $("#to-do-add").val() +"</p></td>\n" +
                "                            <td><div class=\"date-time-todo\"><p>"+ time +" </p><div class=\"date-todo\">"+ date + "</div></div></td>\n " +
                "                            <td> "+ priority +" </td>\n" +
                "                            <td><div class=\"message-received-status-holder\">\n" +
                "                                <i class=\"far fa-comment-alt\"></i>\n" +
                "                                <div class=\"message-received-status message-received-status-bc-1\">2</div>\n" +
                "                            </div></td>\n" +
                "                            <td><div class=\"in-progress-div\">In Progress</div></td>\n" +
                "                        </tr>");

            $(".to-do-list-page .content-table tbody").append(
                "                       <tr class=\"tr-hover-effect\" >\n" +
                "                            <td></td>\n" +
                "                            <td><p>"+ $("#to-do-add").val() +"</p></td>\n" +
                "                            <td><div class=\"date-time-todo\"><p>"+ time +" </p><div class=\"date-todo\">"+ date + "</div></div></td>\n " +
                "                            <td> "+ priority +" </td>\n" +
                "                            <td><div class=\"message-received-status-holder\">\n" +
                "                                <i class=\"far fa-comment-alt\"></i>\n" +
                "                                <div class=\"message-received-status message-received-status-bc-1\">2</div>\n" +
                "                            </div></td>\n" +
                "                            <td><div class=\"in-progress-div\">In Progress</div></td>\n" +
                "                        </tr>");

            $(".to-do-list-edit .content-table tbody").append(
                "                       <tr class=\"tr-hover-effect\" >\n" +
                "                            <td></td>\n" +
                "                            <td><p>"+ $("#to-do-add").val() +"</p></td>\n" +
                "                            <td><div class=\"date-time-todo\"><p>"+ time +" </p><div class=\"date-todo\">"+ date + "</div></div></td>\n " +
                "                            <td> "+ priority +" </td>\n" +
                "                            <td><button type=\"button\" class=\"btn btn-info\">Edit</button></td>\n" +
                "                            <td><button type=\"button\" class=\"btn btn-danger\">Delete</button></td>\n" +
                "                        </tr>");

            $(".to-do-adding-a-comment .content-table tbody").append(
                "                       <tr class=\"tr-hover-effect\" >\n" +
                "                            <td></td>\n" +
                "                            <td><p>"+ $("#to-do-add").val() +"</p><input type=\"text\" class=\"form-for-comment\" placeholder=\"Write a comment here..\"></td>\n" +
                "                            <td><div class=\"date-time-todo\"><p>"+ time +" </p><div class=\"date-todo\">"+ date + "</div></div></td>\n " +
                "                            <td> "+ priority +" </td>\n" +
                "                            <td><button type=\"button\" class=\"btn btn-info\">Save</button></td>\n" +
                "                            <td><button type=\"button\" class=\"btn btn-info\">Cancel</button></td>\n" +
                "                        </tr>");


            getActiveMenu.call("#editing-of-a-task");
            $(".to-do-list, .to-do-list-add, .to-do-list-page, .to-do-list-edit, .to-do-completed-tasks, .to-do-adding-a-comment").hide();
            $(".to-do-list").show();
        }


        $("#to-do-add").val([]);
        $("#to-do-priority option").prop('selected', false);




    });
    $(document).on("click", "#cancel-button", function (){
        $("#to-do-add").val([]);
        removeCurrentTime();
        $("#to-do-priority option").prop('selected', false);
        $("#current-time").show();
    })
    $(document).on("click", "#current-time", function (){
        getCurrentTime();
        $("#current-time").hide();
    })
                                 /* *********************** */

                                 /* THis all for COMPLETED TASKS MENU  */
    $(".to-do-completed-tasks table thead tr th:last-child a").click(function (){
       clearAllElementsTR(".to-do-completed-tasks table tbody tr td");
    });

    $(".to-do-completed-tasks table tbody tr td:first-child i").click(function (){
        getToggleCheck.call(this);
        removeElementOfTableTR.call(this);
    });

                                 /* ************************* */

                                 /* THis all for EDITING OF A TASK MENU  */

    $(document).on("click", ".to-do-list-edit table tbody tr td:nth-child(5) button", function(){
        if ($(".edit-floating-div").css("display") === "none")
        {
            var titleOfTask = $(this).parents("tr").children("td:nth-child(2)").text();
            var time = $(this).parents("tr").find("td .date-time-todo > p:first-child").text().replaceAll(" ", "").replaceAll("\n", "");
            var date = $(this).parents("tr").find("td .date-time-todo > p:last-child").text().replaceAll(" ", "").replaceAll("\n", "");
            var priority = $(this).parents("tr").find("td:nth-child(4) p").text();
            date = (new Date()).getFullYear() + "-" + date[3] + date[4] + "-" + date[0] + date[1];
            switch (priority){
                case "High Priority": priority = 1; break;
                case "Medium Priority": priority = 2; break;
                case "Low Priority": priority = 3; break;
                case "No Priority": priority = 0; break;
            }

            console.log(priority);

            $(".to-do-list-edit table").css("opacity", "0.5");

            $(".edit-floating-div #form-for-name-edit #to-do-add-edit").val(titleOfTask);
            $(".edit-floating-div #form-for-time-edit #to-do-time-edit").val(time);
            $(".edit-floating-div #form-for-date-edit #to-do-due-date-edit").val(date);
            $(".edit-floating-div #form-for-priority-edit #to-do-priority-edit").val(priority).change();

            $(".edit-floating-div").show();
        }
        else
            return false;
    });                             

    $("#cancel-button-edit").click(function (){
        $(".edit-floating-div").hide();
        $(".to-do-list-edit table").css("opacity", "1");
    });

    $(".to-do-list-edit table tbody tr td:not(td:nth-child(5)), .to-do-list-edit table thead").click(function (){
        $(".edit-floating-div").hide();
        $(".to-do-list-edit table").css("opacity", "1");
    })


    $(".to-do-list-edit table tbody tr td:last-child button").click(function (){
        removeElementOfTableTR.call(this)
    });

                                 /* ************************* */

                                 /* THis all for ADDING A COMMENT MENU  */
    $("#adding-a-comment-menu").click(function (){
       $('.to-do-adding-a-comment table tbody tr td button').attr("disabled", 'disabled');
    });
    $(".to-do-adding-a-comment table tbody tr td:nth-child(5) button").click(function (){
       // some login here to save comment;
    });
    $(".to-do-adding-a-comment table tbody tr td:nth-child(6) button").on('click',function (){
       $(this).parents('tr').find('td:nth-child(2) input').val('');
       $(this).parents('tr').find('td button').removeAttr("disabled");
    });
    $(document).on('change',".to-do-adding-a-comment table tbody tr td:nth-child(2) input" , function (){
       $(this).parents('tr').find('td button').removeAttr("disabled");
    });

                                 /* ************************* */
});